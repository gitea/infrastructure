import { CloudFrontClient, CreateInvalidationCommand }  from "@aws-sdk/client-cloudfront";
import { dirname } from "path";

export const handler = async(event) => {
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    console.log(`clearing cache for ${dirname(key)}`);

    const params = {
    	DistributionId: 'TODO: fetch from ENV',
    	InvalidationBatch: {
	        Paths: {
	          Items: [
	              `/${dirname(key)}/`,
	              `/${key}`
	              ],
	          Quantity: 2,
	        },
	        CallerReference: Date.now().toString(),
	      },
    };
    const cfClient = new CloudFrontClient();
    const command = new CreateInvalidationCommand(params)
    const response = await cfClient.send(command)
};

import * as AWS from "@aws-sdk/client-s3";

const s3Client = new AWS.S3Client({});

export const handler = async(event) => {
    
    let path = '';
    if (typeof event.pathParameters !== 'undefined' && typeof event.pathParameters !== 'undefined'){
        path = event.pathParameters.thepath
    }
    
    const params = {
        Bucket: "dl-gitea-com",
        Delimiter: '/',
        Prefix:  path
    };
    
    const listCommand = new AWS.ListObjectsV2Command(params)
    const listedObjects = await s3Client.send(listCommand)
    const files = listedObjects['Contents'];
    const folders = listedObjects['CommonPrefixes'];
    
    const response = {
        statusCode: 200,
        headers: {"content-type": "text/html"},
        body: `${renderTemplFull(files, folders, path)}`,
    };
    return response;
};

var renderTemplFull = (files, folders, path) => {
    return `<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>${renderTemplTitle(path)}</title>
        ${cssStyle}
    </head>
    <body>
    ${svgs}
    <header>
		<h1>
			
				<a href="/">Gitea</a> /
				<!-- breadcrumbs start -->${renderTemplBreadcrumbs(path)}
		</h1>
	</header>
    <main>
			<div class="listing">
				<table aria-describedby="summary">
					<thead>
					<tr>
						<th class="hideable"></th>
						<th class="name">Name</th>
						<th class="description">Description</th>
						<th class="size">Size</th>
						<th class="date hideable">Modified</th>
						<th class="hideable"></th>
					</tr>
					</thead>
					<tbody>
					${renderGoUp(path)}
    <!-- folders start -->${renderTemplFolders(folders)}
    <!-- files start -->${renderTemplFiles(files)}
    </tbody>
				</table>
			</div>
		</main>
		<footer>
			Served by S3 Browser via <a rel="noopener noreferrer" href="https://caddyserver.com">Caddy</a>
		</footer>
	</body>
</html>
    `;
}
var renderGoUp = (path) => {
    if(path !== "") {
         return `
         <tr>
						<td class="hideable"></td>
						<td class="goup">
							<a href="..">
								Go up
							</a>
						</td>
						<td class="description">&mdash;</td>
						<td class="size">&mdash;</td>
						<td class="date hideable">&mdash;</td>
						<td class="hideable"></td>
					</tr>`;
    }
    return '';
}
var renderTemplTitle = (path) => {
    if(path == "") {
        path = '/'
        return `Gitea`
    }
    path = path.slice(0, -1);
    return `Gitea | ${cleanTitle(path)}`;
}

var cleanTitle = (path) => {
	return path.split("/")[0]
}

var renderTemplBreadcrumbs = (path) => {
    var output = '<!-- todo: breadcrumbs -->';
    return output;
}
var renderTemplFolders = (folders) => {
    if(typeof folders === 'undefined') return '';
    folders = semVerSort(folders);
    var output = '';
    for (var i = 0; i < folders.length; i++) {
        if (cleanFolderName(folders[i].Prefix).slice(0,1) == ".") continue;
        output += `<tr class="file ">
							<td class="hideable"></td>
							<td class="name"><a href="/${folders[i].Prefix}"><svg width="1.5em" height="1em" version="1.1" viewBox="0 0 317 259"><use xlink:href="#folder"></use></svg><span class="name">${cleanFolderName(folders[i].Prefix)}</span></a></td>
							<td class="description">&mdash;</td>
							<td class="size">&mdash;</td>
							<td class="date hideable">&mdash;</td>
							<td class="hideable"></td>
						</tr>`;
    }
    return output;
}

const semVerRegex = new RegExp(`^v?([0-9]+)(\.[0-9]+)?(\.[0-9]+)?`)

var semVerSort = (folders) => {
	// return folders;
	var unsorted = [],
		sorted = [];

	for (var i = 0; i < folders.length; i++) {
		var temp = cleanFolderName(folders[i].Prefix),
			len = temp.match(semVerRegex);
		if(len != null && len.length > 0) {
			sorted.push(folders[i])
		} else {
			unsorted.push(folders[i])
		}
	}
	sorted = sorted.sort(function(x, y) {
		var v1 = cleanFolderName(x.Prefix),
			v2 = cleanFolderName(y.Prefix),
			v1min = parseInt(v1.match(semVerRegex)[2].slice(1)),
			v2min = parseInt(v2.match(semVerRegex)[2].slice(1));
		// deal w/ empty patch here
		
		var v1patch = 0,
			v2patch = 0,
			v1patchS1 = v1.match(semVerRegex)[3],
			v2patchS1 = v2.match(semVerRegex)[3];
		if (v1patchS1) {
			v1patch = parseInt(v1patchS1.slice(1))
		}
		if (v2patchS1) {
			v2patch = parseInt(v2patchS1.slice(1))
		}
		if (v1min == v2min && v1patch > v2patch) return -1;
		if (v1min > v2min) return -1;
		
		return 1;
		
	});
	return sorted.concat(unsorted);
}

var renderTemplFiles = (files) => {
    if(typeof files === 'undefined') return '';
    var output = '';
    for (var i = 0; i < files.length; i++) {
        if (files[i].Key.slice(0,1) == ".") continue;
        output += `<tr class="file ">
							<td class="hideable"></td>
							<td class="name"><a href="/${files[i].Key}"><svg width="1.5em" height="1em" version="1.1" viewBox="0 0 265 323"><use xlink:href="#file"></use></svg><span class="name">${cleanFileName(files[i].Key)}</span></a></td>
							<td class="description"></td>
							<td class="size">${humanFileSize(files[i].Size)}</td>
							<td class="date hideable"><time datetime="${files[i].LastModified.toUTCString()}">${files[i].LastModified.toJSON()}</time></td>
							<td class="hideable"></td>
						</tr>`;
    }
    return output;
}

var cleanFileName = (name) => {
	return name.split("/").slice(-1).pop()
}

var cleanFolderName = (name) => {
	return name.slice(0, -1).split("/").slice(-1).pop()
}

// taken from https://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable-string
var humanFileSize = (bytes, si=false, dp=1) => {
  const thresh = si ? 1000 : 1024;

  if (Math.abs(bytes) < thresh) {
    return bytes + ' B';
  }

  const units = si 
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] 
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
  let u = -1;
  const r = 10**dp;

  do {
    bytes /= thresh;
    ++u;
  } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


  return bytes.toFixed(dp) + ' ' + units[u];
}

const svgs = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="0" width="0" style="position: absolute;">
			<defs>
				
				<g id="folder" fill-rule="nonzero" fill="none">
					<path d="M285.22 37.55h-142.6L110.9 0H31.7C14.25 0 0 16.9 0 37.55v75.1h316.92V75.1c0-20.65-14.26-37.55-31.7-37.55z" fill="#FFA000"/>
					<path d="M285.22 36H31.7C14.25 36 0 50.28 0 67.74v158.7c0 17.47 14.26 31.75 31.7 31.75H285.2c17.44 0 31.7-14.3 31.7-31.75V67.75c0-17.47-14.26-31.75-31.7-31.75z" fill="#FFCA28"/>
				</g>
				
				<g id="file" stroke="#000" stroke-width="25" fill="#FFF" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
					<path d="M13 24.12v274.76c0 6.16 5.87 11.12 13.17 11.12H239c7.3 0 13.17-4.96 13.17-11.12V136.15S132.6 13 128.37 13H26.17C18.87 13 13 17.96 13 24.12z"/>
					<path d="M129.37 13L129 113.9c0 10.58 7.26 19.1 16.27 19.1H249L129.37 13z"/>
				</g>
				
				<g id="current" fill-rule="nonzero" fill="none">
					<path style="fill:#ffa000;stroke-width:1.00025" d="M 285.292,37.559481 H 142.656 L 110.92799,1.7359907e-6 H 31.708001 C 14.253597,1.7359907e-6 0,16.904267 0,37.559481 V 112.67844 H 317 V 75.118957 C 317,54.463744 302.73639,37.559481 285.292,37.559481 Z" />
					<path style="fill:#ffca28;stroke-width:1.00025" d="M 285.292,36.009088 H 31.708001 C 14.253597,36.009088 0,50.292693 0,67.757101 V 226.49717 c 0,17.47439 14.263599,31.75801 31.708001,31.75801 H 285.27198 c 17.44441,0 31.70801,-14.30362 31.70801,-31.75801 V 67.767106 c 0,-17.474413 -14.2636,-31.758015 -31.70801,-31.758015 z" />
					<path style="fill:#00aa00;stroke-width:12.1622" d="m 250.19279,113.55876 -97.29813,97.29812 -48.64883,-48.64929 18.24315,-18.24314 30.40568,30.4057 79.05453,-79.054991 z" fill-rule="evenodd" />
				</g>
				
				<g id="working" fill-rule="nonzero" fill="none">
					<path style="fill:#ffa000;stroke-width:1.00025" d="M 285.292,37.559481 H 142.656 L 110.92799,1.7359907e-6 H 31.708001 C 14.253597,1.7359907e-6 0,16.904267 0,37.559481 V 112.67844 H 317 V 75.118957 C 317,54.463744 302.73639,37.559481 285.292,37.559481 Z" />
					<path style="fill:#ffca28;stroke-width:1.00025" d="M 285.292,36.009088 H 31.708001 C 14.253597,36.009088 0,50.292693 0,67.757101 V 226.49717 c 0,17.47439 14.263599,31.75801 31.708001,31.75801 H 285.27198 c 17.44441,0 31.70801,-14.30362 31.70801,-31.75801 V 67.767106 c 0,-17.474413 -14.2636,-31.758015 -31.70801,-31.758015 z" />
					<path style="fill:#7b7b7b;stroke-width:1.00025" d="M 235.76019,164.49959 V 139.5103 h -20.24143 c -1.42673,-6.24505 -3.91685,-12.06575 -7.22395,-17.32448 l 14.34987,-14.34903 -17.68045,-17.666867 -14.34246,14.335447 c -5.26449,-3.30628 -11.08518,-5.79515 -17.33106,-7.2219 V 77.040811 H 148.30142 V 97.28347 c -6.24711,1.42675 -12.06656,3.91562 -17.33231,7.23384 L 116.62008,90.169923 98.953224,107.83679 113.30267,122.18582 c -3.31205,5.25873 -5.80216,11.07943 -7.22972,17.32448 H 85.831113 v 24.98929 h 20.241427 c 1.42756,6.2471 3.91109,12.06739 7.22395,17.32654 l -14.343266,14.34779 17.668916,17.68045 14.34697,-14.34986 c 5.26575,3.30709 11.0852,5.7972 17.33231,7.22396 v 20.24143 h 24.98929 v -20.24143 c 6.24628,-1.42676 12.05956,-3.91687 17.32571,-7.22396 l 14.34781,14.34986 17.68045,-17.68045 -14.34987,-14.34779 c 3.3071,-5.27233 5.79722,-11.07944 7.22395,-17.32654 z m -74.96413,18.74175 c -17.25238,0 -31.23597,-13.98442 -31.23597,-31.23599 0,-17.25362 13.98359,-31.23434 31.23597,-31.23434 17.25239,0 31.23516,13.98072 31.23516,31.23434 4e-4,17.25116 -13.98236,31.23599 -31.23516,31.23599 z" />
				</g>
				
				<g id="release" fill-rule="nonzero" fill="none">
					<path style="fill:#ffd48b;stroke-width:1.00025" d="M 285.292,37.559481 H 142.656 L 110.92799,1.7359907e-6 H 31.708001 C 14.253597,1.7359907e-6 0,16.904267 0,37.559481 V 112.67844 H 317 V 75.118957 C 317,54.463744 302.73639,37.559481 285.292,37.559481 Z" />
					<path style="fill:#ffe596;stroke-width:1.00025" d="M 285.292,36.009088 H 31.708001 C 14.253597,36.009088 0,50.292693 0,67.757101 V 226.49717 c 0,17.47439 14.263599,31.75801 31.708001,31.75801 H 285.27198 c 17.44441,0 31.70801,-14.30362 31.70801,-31.75801 V 67.767106 c 0,-17.474413 -14.2636,-31.758015 -31.70801,-31.758015 z" />
					<path style="fill:#8abc8a;stroke-width:12.1622" d="m 250.19279,113.55876 -97.29813,97.29812 -48.64883,-48.64929 18.24315,-18.24314 30.40568,30.4057 79.05453,-79.054991 z" />
				</g>
				
				<g id="older" fill-rule="nonzero" fill="none">
					<path style="fill:#808080;stroke-width:1.00025" d="M 285.292,37.559481 H 142.656 L 110.92799,1.7359907e-6 H 31.708001 C 14.253597,1.7359907e-6 0,16.904267 0,37.559481 V 112.67844 H 317 V 75.118957 C 317,54.463744 302.73639,37.559481 285.292,37.559481 Z" />
					<path style="fill:#cccccc;stroke-width:1.00025" d="M 285.292,36.009088 H 31.708001 C 14.253597,36.009088 0,50.292693 0,67.757101 V 226.49717 c 0,17.47439 14.263599,31.75801 31.708001,31.75801 H 285.27198 c 17.44441,0 31.70801,-14.30362 31.70801,-31.75801 V 67.767106 c 0,-17.474413 -14.2636,-31.758015 -31.70801,-31.758015 z" />
					<path style="fill:#808080;stroke-width:1.00025" d="M 235.76019,164.49959 V 139.5103 h -20.24143 c -1.42673,-6.24505 -3.91685,-12.06575 -7.22395,-17.32448 l 14.34987,-14.34903 -17.68045,-17.666867 -14.34246,14.335447 c -5.26449,-3.30628 -11.08518,-5.79515 -17.33106,-7.2219 V 77.040811 H 148.30142 V 97.28347 c -6.24711,1.42675 -12.06656,3.91562 -17.33231,7.23384 L 116.62008,90.169923 98.953224,107.83679 113.30267,122.18582 c -3.31205,5.25873 -5.80216,11.07943 -7.22972,17.32448 H 85.831113 v 24.98929 h 20.241427 c 1.42756,6.2471 3.91109,12.06739 7.22395,17.32654 l -14.343266,14.34779 17.668916,17.68045 14.34697,-14.34986 c 5.26575,3.30709 11.0852,5.7972 17.33231,7.22396 v 20.24143 h 24.98929 v -20.24143 c 6.24628,-1.42676 12.05956,-3.91687 17.32571,-7.22396 l 14.34781,14.34986 17.68045,-17.68045 -14.34987,-14.34779 c 3.3071,-5.27233 5.79722,-11.07944 7.22395,-17.32654 z m -74.96413,18.74175 c -17.25238,0 -31.23597,-13.98442 -31.23597,-31.23599 0,-17.25362 13.98359,-31.23434 31.23597,-31.23434 17.25239,0 31.23516,13.98072 31.23516,31.23434 4e-4,17.25116 -13.98236,31.23599 -31.23516,31.23599 z" />
				</g>
			</defs>
		</svg>`;
const cssStyle = `<style>
* { padding: 0; margin: 0; }
body {
	font-family: sans-serif;
	text-rendering: optimizespeed;
	background-color: #ffffff;
}
a {
	color: #006ed3;
	text-decoration: none;
}
a:hover,
h1 a:hover {
	color: #319cff;
}
header,
#summary {
	padding-left: 5%;
	padding-right: 5%;
}
th:first-child,
td:first-child {
	width: 5%;
}
th:last-child,
td:last-child {
	width: 5%;
}
header {
	padding-top: 25px;
	padding-bottom: 15px;
	background-color: #f2f2f2;
}
h1 {
	font-size: 20px;
	font-weight: normal;
	white-space: nowrap;
	overflow-x: hidden;
	text-overflow: ellipsis;
	color: #999;
}
h1 a {
	color: #000;
	margin: 0 4px;
}
h1 a:hover {
	text-decoration: underline;
}
h1 a:first-child {
	margin: 0;
}
main {
	display: block;
}
.meta {
	font-size: 12px;
	font-family: Verdana, sans-serif;
	border-bottom: 1px solid #9C9C9C;
	padding-top: 10px;
	padding-bottom: 10px;
}
.meta-item {
	margin-right: 1em;
}
#filter {
	padding: 4px;
	border: 1px solid #CCC;
}
table {
	width: 100%;
	border-collapse: collapse;
}
tr {
	border-bottom: 1px dashed #dadada;
}
tr.current {
	background-color: #eeffee;
}
tbody tr:hover {
	background-color: #ffffec;
}
th,
td {
	text-align: left;
	padding: 10px;
	vertical-align: text-top;
}
th {
	padding: 15px 10px;
	font-size: 16px;
	white-space: nowrap;
}
th a {
	color: black;
}
th svg {
	vertical-align: middle;
}
td {
	white-space: nowrap;
	font-size: 14px;
}
td.description {
	width: auto;
}
th.size,
td.size {
	text-align: right;
}
td.name svg {
	position: absolute;
	font-size: 140%;
	margin-top: -0.1ex;
}
td .name,
td .goup {
	margin-left: 2.35em;
	white-space: pre;
}
.icon {
	margin-right: 5px;
}
.icon.sort {
	display: inline-block;
	width: 1em;
	height: 1em;
	position: relative;
	top: .2em;
}
.icon.sort .top {
	position: absolute;
	left: 0;
	top: -1px;
}
.icon.sort .bottom {
	position: absolute;
	bottom: -1px;
	left: 0;
}
footer {
	padding: 40px 20px;
	font-size: 12px;
	text-align: center;
}
@media (max-width: 600px) {
	.hideable {
		display: none;
	}
	h1 {
		color: #000;
	}
	h1 a {
		margin: 0;
	}
	#filter {
		max-width: 100px;
	}
}
</style>`;